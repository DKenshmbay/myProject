export const environment = {
    production: true,
    hmr       : false,
    apiUrl: 'http://portal.ttservice.kz:83',
    apiApplicationUrl: 'http://portal.ttservice.kz:82',
    apiUploadFile: 'http://portal.ttservice.kz:84',
    apiAdminUrl: 'http://portal.ttservice.kz:85',
};
