import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applocation',
        title    : 'СТАТИСТИКА',
        translate: 'NAV.APPLICATIONS',
        type     : 'group',
        children : [
            
            {
                id       : 'График просрочки',
                title    : 'График просрочки',
                translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'dashboard',
                url      : '/',
                
            }
        ]
    },
    {
        id       : 'applications',
        title    : 'ЗАЯВКИ НА РЕМОНТ',
        translate: 'ЗАЯВКИ НА РЕМОНТ',
        type     : 'group',
        children : [
            
            {
                id       : 'all',
                title    : 'Все заявки',
                translate: 'Все заявки',
                type     : 'item',
                icon     : 'directions_transit',
                url      : '/applications/All',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'draft',
                title    : 'Черновик',
                translate: 'Черновик',
                type     : 'item',
                icon     : 'menu',
                url      : '/applications/Draft',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'inWork',
                title    : 'В работе',
                translate: 'В работе',
                type     : 'item',
                icon     : 'work',
                url      : '/applications/InWork',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'inRepair',
                title    : 'В ремонте',
                translate: 'В ремонте',
                type     : 'item',
                icon     : 'build',
                url      : '/applications/InRepair',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'documentCollect',
                title    : 'Сбор документации',
                translate: 'Сбор документации',
                type     : 'item',
                icon     : 'collections_bookmark',
                url      : '/applications/DocumentCollect',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'agreementOpd',
                title    : 'Согласование ОПД',
                translate: 'Согласование ОПД',
                type     : 'item',
                icon     : 'done_all',
                url      : '/applications/AgreementOpd',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'agreementITSZHT',
                title    : 'Согласование ИЦЖТ',
                translate: 'Согласование ИЦЖТ',
                type     : 'item',
                icon     : 'done_all',
                url      : '/applications/AgreementITSZHT',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'agreementTso',
                title    : 'Согласование ЦО',
                translate: 'Согласование ЦО',
                type     : 'item',
                icon     : 'done_all',
                url      : '/applications/AgreementTso',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'paymentFormation',
                title    : 'Формирование заявки на оплату',
                translate: 'Формирование заявки на оплату',
                type     : 'item',
                icon     : 'add_to_photos',
                url      : '/applications/PaymentFormation',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'payment',
                title    : 'На оплате',
                translate: 'На оплате',
                type     : 'item',
                icon     : 'local_atm',
                url      : '/applications/Payment',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            {
                id       : 'paid',
                title    : 'Оплачена',
                translate: 'Оплачена',
                type     : 'item',
                icon     : 'payment',
                url      : '/applications/Paid',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            },
            
            {
                id       : 'reWork',
                title    : 'На доработке',
                translate: 'На доработке',
                type     : 'item',
                icon     : 'loop',
                url      : '/applications/ReWork',
                badge    : {
                    title    : '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg       : '#F44336',
                    fg       : '#FFFFFF'
                }
            }
        ]
    },
    {
        id       : 'manager',
        title    : 'МЕНЕДЖЕР',
        translate: 'МЕНЕДЖЕР',
        type     : 'group',
        children : [
            
            {
                id       : 'Договоры',
                title    : 'Договоры',
                translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'work',
                url      : '/contract-list',
                
            }
        ]
    },
    {
        id       : 'manager',
        title    : 'АДМИНИСТРИРОВАНИЕ',
        translate: 'АДМИНИСТРИРОВАНИЕ',
        type     : 'group',
        children : [
            
            {
                id       : 'Пользователи',
                title    : 'Пользователи',
                translate: 'NAV.SAMPLE.TITLE',
                type     : 'item',
                icon     : 'supervisor_account',
                url      : '/admin',
                
            },
            {
                id       : 'Справочник Станции',
                title    : 'Справочник Станции',
                translate: 'Справочник Станции',
                type     : 'item',
                icon     : 'list',
                url      : '/dictionaries'
            },
            {
                id       : 'archived',
                title    : 'Архив',
                translate: 'Архив',
                type     : 'item',
                icon     : 'archive',
                url      : '/applications/Archived',
                // badge    : {
                //     title    : '25',
                //     translate: 'NAV.SAMPLE.BADGE',
                //     bg       : '#F44336',
                //     fg       : '#FFFFFF'
                // }
            }
        ]
    },
    
];
