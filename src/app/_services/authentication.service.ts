﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import jwtDecode from 'jwt-decode';
import { environment } from 'environments/environment';
import { IDecodedToken} from '../interfaces/decoded-token.interface';
import { Roles } from '../enums/roles.enum';
import { ITokenResult } from '../interfaces/token.interface';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<IDecodedToken>;
    public currentUser: Observable<IDecodedToken>;
    private readonly tokenKey = 'accessToken';
    private readonly refreshKey = 'refreshToken';
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<IDecodedToken>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): IDecodedToken {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/api/Account/login`, { login:username, password })
            .pipe(map(user => {
                this._setLocalStorage(user);
                var _user = this._decode();
                localStorage.setItem('currentUser', JSON.stringify(_user));
                this.currentUserSubject.next(_user);
                return user;
            }));
    }
    private _setLocalStorage = (result: ITokenResult) => {
        if (!result) return;
        localStorage.setItem(this.tokenKey, result.accessToken);
        localStorage.setItem(this.refreshKey, result.refreshToken);
      };
    private _decode = () => {
        const token = localStorage.getItem(this.tokenKey);
        const decoded = token ? jwtDecode<IDecodedToken>(token) : null;
        if (!decoded) return null;
        return {
          ...decoded,
          expires: new Date(decoded.exp),
          notBefore: new Date(decoded.nbf),
          roles: decoded.roles as Roles[],
          accessToken:token,
          fullName: decoded.firstName +" "+decoded.lastName+" "+decoded.middleName
        };
      };

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}