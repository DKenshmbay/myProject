
  import { Injectable } from '@angular/core';  
  import {  Workbook } from 'exceljs';  
  import * as fs from 'file-saver';  
  import * as moment from 'moment'  
  @Injectable()  
  export class ExcelService {  
      constructor() {}  
      async generateExcel(data) {  
          const header = ['Номер заявки', 'Номер вагона', 'С заменой','Груженый/порожний','Станция отцепки', 'Дата отцепки', 'Контрагент', 'Статус', 'Срок','ВЧД','Дата подачи заявки'];  
          // Create workbook and worksheet  
          const workbook = new Workbook();  
          const worksheet = workbook.addWorksheet();  
          // Cell Style : Fill and Header  
          var TodayDate = new Date();  
          let MMDDYY = moment(TodayDate).format('MMDDYY').toString();  
          var FileName = "ExportuserData" + MMDDYY;  
          const headerRow = worksheet.addRow(header);  
          headerRow.height = 30;
          headerRow.eachCell((cell, number) => {  
              cell.alignment = { vertical: 'middle', horizontal: 'center' }
              cell.fill = {  
                  type: 'pattern',  
                  pattern: 'solid',  
                  fgColor: {  
                      argb: 'ececec'  
                  },  
                  bgColor: {  
                      argb: 'FFFFFFFF'  
                  },  
              };  
              cell.font = {  
                  color: {  
                      argb: '00000000',  
                  },  
                  bold: true  
              }  
              cell.border = {  
                  top: {  
                      style: 'thin'  
                  },  
                  left: {  
                      style: 'thin'  
                  },  
                  bottom: {  
                      style: 'thin'  
                  },  
                  right: {  
                      style: 'thin'  
                  }  
              };  
          });  
          data.forEach(d => {  
              const row = worksheet.addRow([d.number,d.carriageNumber,d.withReplacement?'Да':'Нет',d.loadedMark==1?'Груженый':'Порожний',d.detachName,d.releaseDate,d.contractorsName,d.statusTitle,d.finishDate,d.repairPlaceName?d.repairPlaceName:"",d.taskDate]);  
              row.fill = {  
                  type: 'pattern',  
                  pattern: 'solid',  
                  fgColor: {  
                      argb: 'FFFFFFFF'  
                  }  
              };  
              
              row.alignment = { vertical: 'top', horizontal: 'left' };
              row.font = {  
                  color: {  
                      argb: '00000000',  
                  },  
                  bold: false  
              }  
              row.eachCell((cell, number) => {  
                  cell.border = {  
                      top: {  
                          style: 'thin'  
                      },  
                      left: {  
                          style: 'thin'  
                      },  
                      bottom: {  
                          style: 'thin'  
                      },  
                      right: {  
                          style: 'thin'  
                      }  
                  };  
              });  
          });  

        function eachColumnInRange(ws, col1, col2, cb){
          for(let c = col1; c <= col2; c++){
              let col = ws.getColumn(c);
              cb(col);
          }
        }
        

        function autofitColumns(ws){
            eachColumnInRange(ws, 1, ws.columnCount, column => {
                
                let maxWidth=10;
                column.eachCell( cell => {
                    if( !cell.isMerged && cell.value ){ 
                        
                        let text = "";
                        if( typeof cell.value != "object" ){
                            text = cell.value.toString();
                        } else if( cell.value.richText ){
                            text = cell.value.richText.reduce((text, obj)=>text+obj.text.toString(),"");
                        }
        
                        let values = text.split(/[\n\r]+/);
                        
                        for( let value of values ){
                            let width = value.length;
                            
                            if(cell.font && cell.font.bold){
                                width *= 1.08;
                            }
                            
                            maxWidth = Math.max(maxWidth, width);
                        }
                    }
                });
                maxWidth += 0.71;
                maxWidth += 1; 
                column.width = maxWidth;
                column.height = 50;
            });
        }
    
        autofitColumns(worksheet);

        workbook.xlsx.writeBuffer().then((data: any) => {  
            const blob = new Blob([data], {  
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'  
            });  
            fs.saveAs(blob, FileName + '.xlsx');  
        });  
      }  
  }  
