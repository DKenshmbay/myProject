import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http:HttpClient) {}
   getUsers() {
      return this.http.get<any>(`${environment.apiAdminUrl}/api/User/GetAllUsers`);
   };
   getRoles() {
    return this.http.get<any>(`${environment.apiAdminUrl}/api/User/Roles`);
  };
  getUser(login:string) {
    return this.http.get<any>(`${environment.apiAdminUrl}/api/User/GetUser/`+login);
  }
  addUser(user:any) {
    return this.http.post<any>(`${environment.apiAdminUrl}/api/User/AddUser`, user)
        .pipe(map(result => {
            return result;
        }));
  }
  updateUser(user:any) {
    return this.http.post<any>(`${environment.apiAdminUrl}/api/User/UpdateUser`, user)
        .pipe(map(result => {
            return result;
        }));
  }
  deleteUser(login:string) {
    return this.http.get<any>(`${environment.apiAdminUrl}/api/User/DeleteUser/`+login);
  }
}
