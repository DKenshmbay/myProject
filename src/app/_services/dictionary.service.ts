import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { delay, map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  constructor(private http:HttpClient) { }
  getAll(){
    return this.http.get<any>(`${environment.apiAdminUrl}/api/Station/GetAll/`);
  }

  getStation(stationId:string){
    return this.http.get<any>(`${environment.apiAdminUrl}/api/Station/GetStation/` + stationId);
  }

  deleteStation(stationId:string){
    return this.http.get<any>(`${environment.apiAdminUrl}/api/Station/DeleteStation/` + stationId);
  }

  addOrUpdate(data:any){
    return this.http.post<any>(`${environment.apiAdminUrl}/api/Station/AddOrUpdate`, data)
    .pipe(map(result => {
      return result;
  }));;
  }
}
