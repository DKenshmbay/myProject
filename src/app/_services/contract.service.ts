import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ContractService {
    constructor(private http:HttpClient) {}
    getContracts() {
       return this.http.get<any>(`${environment.apiApplicationUrl}/api/Contract/GetAll`);
    };
    getContract(id:any) {
        return this.http.get<any>(`${environment.apiApplicationUrl}/api/Contract/Get/`+id);
     };

    addContract(contract:any) {
        return this.http.post<any>(`${environment.apiApplicationUrl}/api/Contract/AddOrUpdate`, contract)
            .pipe(map(result => {
                return result;
            }));
    };

    deleteContract(id:any) {
        return this.http.get<any>(`${environment.apiApplicationUrl}/api/Contract/Delete/`+id);
    };
  
}
