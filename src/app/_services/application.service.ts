import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Applications } from 'app/interfaces/applications.interface';

export interface Person {
  id: string;
  isActive: boolean;
  age: number;
  name: string;
  gender: string;
  company: string;
  email: string;
  phone: string;
  disabled?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(private http:HttpClient) {}
    getDictionary(name: string) {
        return this.http.get<any>(`${environment.apiApplicationUrl}/api/Dictionary/GetDictionaryItems/`+name);
    }
    getCharts() {
      return this.http.get<any>(`${environment.apiApplicationUrl}/api/Charts/GetExpiredContractors`);
    }
    getApplications(status:number) {
        return this.http.get<any>(`${environment.apiApplicationUrl}/api/Application/GetApplications/`+status);
    }
    getApplicationById(id:string) {
      return this.http.get<any>(`${environment.apiApplicationUrl}/api/Application/`+id);
    }
    removeApplicationById(id:string) {
      return this.http.get<any>(`${environment.apiApplicationUrl}/api/Application/DeleteApplication/`+id);
    }
    sendAppliation(application: Applications) {
        return this.http.post<any>(`${environment.apiApplicationUrl}/api/Application/InsertOrUpdateApplication`, application)
            .pipe(map(result => {
                return result;
            }));
    }
    sendToUser(data: any) {
        return this.http.post<any>(`${environment.apiApplicationUrl}/api/Application/SendToUser`, data)
            .pipe(map(result => {
                return result;
            }));
    }
    
    getRemarks(id:string) {
      return this.http.get<any>(`${environment.apiApplicationUrl}/api/Application/GetRemarks/`+id);
    }
    addRemark(remark) {
      return this.http.post<any>(`${environment.apiApplicationUrl}/api/Application/InsertOrUpdateRemark`, remark)
          .pipe(map(result => {
              return result;
          }));
    }
    removeRemarkById(id:string) {
      return this.http.get<any>(`${environment.apiApplicationUrl}/api/Application/DeleteRemark/`+id);
    }

    setNavigationsCount() {
      return this.http.get<any>(`${environment.apiApplicationUrl}/api/Application/Statistics`);
    }
    removeApplicationFile(id:string) {
      return this.http.get<any>(`${environment.apiUploadFile}/api/File/DeleteFiles/`+id);
    }
    getApplicationFilesById(id:string) {
      return this.http.get<any>(`${environment.apiUploadFile}/api/File/List/`+id);
    }
    getPdfFilesById(id:string) {
      return this.http.get(`${environment.apiUploadFile}/api/File/FileToPdf/`+id, { responseType: 'blob' }).pipe(map(
        (res) => {
          return new Blob([res], { type: "application/pdf" });
        }));
    }

    getApplicationHistory(id:string) {
      return this.http.get<any>(`${environment.apiApplicationUrl}/api/History/GetApplicationHistory?applicationId=`+id);
    }
}
