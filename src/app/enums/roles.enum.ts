export enum Roles{
        /// <summary>
        /// Администратор
        /// </summary>
        Admin = 1,
        /// <summary>
        /// Тор специалист
        /// </summary>
        TorSpecialist,
        /// <summary>
        /// Экономист
        /// </summary>
        Economist,
        /// <summary>
        /// отдел продаж
        /// </summary>
        SalesDepartment,
        /// <summary>
        /// ревизиональная служба
        /// </summary>
        AuditService,

        /// <summary>
        /// Контрагент
        /// </summary>
        Counterparty,

        /// <summary>
        /// Менеджер отдела тор
        /// </summary>
        TorManager,

        /// <summary>
        /// Казначей
        /// </summary>
        Treasurer,
       
        /// <summary>
        /// Может просматривать
        /// </summary>
        Viewer
}
