export enum RepairType
    {
        /// <summary>
        /// Эксплуатационный
        /// </summary>
        Operational = 1,

        /// <summary>
        /// Рекламационный
        /// </summary>
        Complaint
    }