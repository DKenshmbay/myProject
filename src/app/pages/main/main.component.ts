import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ApplicationService } from 'app/_services/application.service'
import { ApplicationStatus } from 'app/enums/applicationStatus.enum';
import { MatSnackBar } from '@angular/material/snack-bar'; 
import { AppComponent } from '../../app.component';
import { AuthenticationService,ExcelService } from 'app/_services';
import { Roles } from 'app/enums/roles.enum';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Applications } from '../../interfaces/applications.interface';
import { environment } from 'environments/environment';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements AfterViewInit  {
    // const paginatorIntl = new MatPaginatorIntl();
    selection = new SelectionModel<Applications>(true, []);
    link:string;
    displayedColumns: string[] = ['select','number', 'carriageNumber','withReplacement','loadedMark','releaseDate','contractorsName','statusTitle','finishDate','options'];
    dataSource: MatTableDataSource<Applications>;
  
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    // rows: any[];

    rows: any[];
    currentStatus:ApplicationStatus;
    loadingIndicator: boolean;
    isEdit: boolean;
    reorderable: boolean;
    public currentUserRoles: string;
    public get Roles(): typeof Roles {
        return Roles; 
      }
    public get Status(): typeof ApplicationStatus {
        return ApplicationStatus; 
    }
    /**
     * Constructor
     *
     */
    constructor(
        private router: Router,
        private applicationService:ApplicationService,
        private route:ActivatedRoute,
        private snackBar:MatSnackBar,
        public myapp: AppComponent,
        private excelService: ExcelService,
        private authenticationService: AuthenticationService
    )
    {
        this.loadingIndicator = true;
        this.reorderable = true;
        this.authenticationService.currentUser.subscribe(x => 
            {
                if(x)
                this.currentUserRoles = x.roles.toString()
            });
        this.link = `${environment.apiUploadFile}/api/Print/GenerateFineReport`;
            
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected == numRows;
    }
    
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }
    isInRole(role:Roles)
    {
      if(this.currentUserRoles)
          return  JSON.parse(this.currentUserRoles).some(a=>a==role);
    }
    isInRoles(roles:Roles[])
    {
      if(this.currentUserRoles)
          return  JSON.parse(this.currentUserRoles).some(a=>roles.includes(a));
    }
    add(){
        this.router.navigate(['/application/create']);
    }
    edit(id:string){
        this.router.navigate(['/application/edit',id]);
    }
    remove(id:string){
        this.applicationService.removeApplicationById(id).subscribe(x => {
            this.myapp.updateBadgeCounts();
            this.loadApplication(ApplicationStatus[this.route.snapshot.url[1].path]);
            this.snackBar.open('Заявка удалена!', '', {
                duration: 2000,
              });
        })
    }

    /**
     * On init
     */
    ngAfterViewInit() {
        this.loadingIndicator = false;
    }
    ngOnInit(): void
    {
      this.currentStatus = ApplicationStatus[this.route.snapshot.url[1].path];
      this.loadingIndicator = false;
      this.loadApplication(ApplicationStatus[this.route.snapshot.url[1].path]);
 
    
      switch(this.currentStatus) { 
        case ApplicationStatus.InWork: { 
            if(this.isInRoles([Roles.Economist,Roles.SalesDepartment,
                Roles.AuditService,Roles.TorManager,Roles.Treasurer]))
                    this.isEdit = true; 
           break; 
        } 
        case ApplicationStatus.InRepair: 
        case ApplicationStatus.DocumentCollect: 
        case ApplicationStatus.ReWork: 
        {
            if(this.isInRole(Roles.Counterparty))
                this.isEdit = true; 
            break; 
        }
        case ApplicationStatus.Draft:
            {
                if(this.isInRole(Roles.TorSpecialist))
                    this.isEdit = true; 
                break; 
            } 
     }
     
        if(!(this.isInRoles([Roles.TorSpecialist,Roles.Admin]) && (this.Status.Draft == this.currentStatus || this.isInRole(Roles.Admin))))
                this.displayedColumns.splice(this.displayedColumns.indexOf('options'),1)
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }

    loadApplication(status:number){
        this.applicationService.getApplications(status).subscribe(result => {
            console.log(result.items)
            this.dataSource = new MatTableDataSource(result.items);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });
    }

    downloadExcel(): void {  
        if(this.dataSource)
            this.excelService.generateExcel(this.dataSource.filteredData);  
    }  
}
