import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl  } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Contracts } from 'app/interfaces/contracts.interface';
import { ContractService } from 'app/_services/contract.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { first } from 'lodash';
export const MY_FORMATS = {
    parse: {
      dateInput: 'DD.MM.YYYY',
    },
    display: {
      dateInput: 'DD.MM.YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
  };

@Component({
  selector: 'contract-component',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss'],
  providers: [{
    provide: MAT_DATE_LOCALE,
    useValue: 'ru-RU'
  },
  {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  },
  {
      provide: MAT_DATE_FORMATS,
      useValue: MY_FORMATS
  }
]
})
export class ContractComponent implements OnInit {
    dialogData:any;
    contractorsList:any[];
    contractorStatusList:any[];
    roles:any[];
    isAdd:boolean;
    form: FormGroup;
    formContract:Contracts;
    minDate:Date = new Date();
    
    constructor(public dialog: MatDialogRef<ContractComponent>,
        private _formBuilder: FormBuilder,
        private cotractService:ContractService,
        private snackBar:MatSnackBar,
                @Inject(MAT_DIALOG_DATA) public data: any) { }
  
    ngOnInit(): void {
      this.dialogData =  this.data.Data;
      this.contractorsList = this.data.contractorsList;
      this.contractorStatusList=this.data.contractorStatusList
      this.isAdd = this.data.isAdd;
      this.initForm();
      this.loadData();
    }
    onNoClick(): void {
      this.dialog.close();
    }

    onSaveClick(){
        this.form.markAllAsTouched();
        if(this.form.valid)
        {
            this.formContract = this.form.getRawValue();
            this.cotractService.addContract(this.formContract).subscribe(result => {
                this.snackBar.open('Запись сохранена!', '', {
                  duration: 2000,
                });
                this.dialog.close();
             });
        }

      };

    initForm(){
        this.form = this._formBuilder.group({
            id: [''],
            contractorId: ['', Validators.required],
            number: ['', Validators.required],
            singDate: ['', Validators.required],
            statusId: [''],
            finisDate: [''],
            note: ['']
        });

    }
    loadData(){
        if(this.dialogData.Id){
            this.cotractService.getContract(this.dialogData.Id)
            .pipe().subscribe(x => 
              {
                  this.form.patchValue(x);
                  this.form.get('contractorId').disable();
              });
          }
          else{
            this.form.patchValue({finisDate: new Date(Date.UTC(new Date().getFullYear(), 11, 31, 0, 0, 0)).toISOString()});
          }
    }
}
