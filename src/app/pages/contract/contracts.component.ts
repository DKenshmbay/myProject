import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from 'app/_services/application.service';
import { ContractService } from 'app/_services/contract.service';
import { ContractComponent } from './contract/contract.component';

@Component({
  selector: 'contract',
  templateUrl: './contracts.component.html',
  styleUrls: ['./contracts.component.sass']
})
export class ContractsComponent implements OnInit {
    
    contracts: any[];
    contractorsList: any[];
    contractorStatusList: any[];
    loadingIndicator: boolean;
    reorderable: boolean;
    dialogData: any
    /**
     * Constructor
     *
     */
    constructor(
        private router: Router,
        private route:ActivatedRoute,
        private cotractService:ContractService,
        private applicationService: ApplicationService,
        private snackBar:MatSnackBar,
        public dialog: MatDialog,
    )
    {
        this.loadingIndicator = true;
        this.reorderable = true;
    }
    add(){
        const dialogRef = this.dialog.open(ContractComponent, {
            width: '50%',
            data: {Data:{},
             contractorsList:this.contractorsList,
             contractorStatusList:this.contractorStatusList,
             isAdd:true}
          });
          dialogRef.afterClosed().subscribe(result => {
            this.loadData();
        });
    }

    edit(item:any){
        const dialogRef = this.dialog.open(ContractComponent, {
          width: '50%',
          data: {Data:
          {
            Id:item.id
          },
          contractorsList:this.contractorsList, 
          contractorStatusList:this.contractorStatusList,
          isAdd:false}
        });
        dialogRef.afterClosed().subscribe(result => {
            this.loadData();
        });
    }


    remove(id:string){
        this.cotractService.deleteContract(id).subscribe(x => {
            this.snackBar.open('Договор удален!', '', {
                duration: 2000,
              });
        this.loadData();
        })
    }
  ngOnInit(): void {
    this.loadDicContractors();
    this.loadContractorStatus();
    this.loadData();
  }
  loadData(){
    this.cotractService.getContracts().subscribe(result => {
        this.contracts = result;
        this.loadingIndicator = false;
    });
 }

 loadDicContractors(){
    this.applicationService.getDictionary("DicContractors").subscribe(result => {
        this.contractorsList= result;
    });
 }
 loadContractorStatus(){
    this.applicationService.getDictionary("DicContractStatus").subscribe(result => {
        this.contractorStatusList= result;
    });
 }
}
