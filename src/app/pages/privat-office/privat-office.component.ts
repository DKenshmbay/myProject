import { Component, OnInit } from '@angular/core';
import { AdminService } from 'app/_services/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IDecodedToken } from 'app/interfaces/decoded-token.interface';
import { AuthenticationService } from 'app/_services';
import { result } from 'lodash';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { NewPasswordComponent } from '../privat-office/new-password/new-password.component';

@Component({
  selector: 'app-privat-office',
  templateUrl: './privat-office.component.html',
  styleUrls: ['./privat-office.component.scss']
})
export class PrivatOfficeComponent implements OnInit {
  inf: any = {};
  users: any[];
  roles: any[];
  loadingIndicator: boolean;
  public currentUser: IDecodedToken;

  constructor(private router: Router,
              private adminService:AdminService,
              private snackBar:MatSnackBar,
              public dialog: MatDialog,
              private authenticationService: AuthenticationService) { 
                this.authenticationService.currentUser.subscribe(x => 
                  {
                      this.currentUser = x
                  });

              }

ngOnInit(): void {
  this.loadRoles();
  this.info();
  
}

info(){
  this.adminService.getUser(this.currentUser.login).subscribe(result=>{
    this.inf = result;
    console.log(this.inf);
  })
}

loadRoles(){
  this.adminService.getRoles().subscribe(result => {
  this.roles = result;
  this.loadingIndicator = false;
    });
 }

save(){
  this.adminService.updateUser(this.currentUser.login).subscribe(added => {
    this.snackBar.open('Запись сохранено!', '', {
      duration: 2000,
    });
    this.info();
 });
} 

open(){
  const dialogRef = this.dialog.open(NewPasswordComponent, {
      width: '50%',
      data: {}
    });
  dialogRef.afterClosed().subscribe(result => {
    if(result){
      this.snackBar.open('Станция добавлена!', '', {
        duration: 2000,
      });

    }
  });
}
}
