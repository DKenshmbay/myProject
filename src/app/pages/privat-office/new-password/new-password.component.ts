import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AdminService } from 'app/_services/admin.service';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnInit {
  users: any[];
  
  roles: any[];
  loadingIndicator: boolean;
  reorderable: boolean;
  dialogData: any={};
  constructor(public dialog: MatDialog,
             private snackBar:MatSnackBar,
             private adminService:AdminService) { }

  ngOnInit(): void {
    this.loadData();
    this.loadRoles();
  }

  edit(item:any){
    const dialogRef = this.dialog.open(NewPasswordComponent, {
      width: '50%',
      data: {userData:
      {
        login:item.login,
        email:item.email,
        lastName:item.lastName,
        firstName:item.firstName,
        middleName:item.middleName,
        roleId:item.roleId,
        contractorId:item.contractorId,
        isBlocked:item.isBlocked
      },
       roles:this.roles}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.adminService.updateUser(result).subscribe(added => {
          this.snackBar.open('Запись сохранено!', '', {
            duration: 2000,
          });
          this.loadData();
       });
      }
    });
}


loadData(){
  this.adminService.getUsers().subscribe(result => {
      this.users = result;
      this.loadingIndicator = false;
  });
}
loadRoles(){
  this.adminService.getRoles().subscribe(result => {
      this.roles = result;
      this.loadingIndicator = false;
  });
}
}
