import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivatOfficeComponent } from './privat-office.component';

describe('PrivatOfficeComponent', () => {
  let component: PrivatOfficeComponent;
  let fixture: ComponentFixture<PrivatOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivatOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivatOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
