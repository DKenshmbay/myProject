
export const Reclamation: any[] = [
    {
        id       : 1,
        title    : 'Акт выполненных работ',
        required : false
    },
    {
        id       : 2,
        title    : 'Счет-фактура',
        required : false
    },{
        id       : 3,
        title    : 'Акт формы ВУ-36',
        required : false
    },{
        id       : 4,
        title    : 'Акт формы ВУ-23',
        required : false
    },{
        id       : 5,
        title    : 'Справка 2612',
        required : false
    },{
        id       : 6,
        title    : 'Дефектная ведомость ВУ-22',
        required : false
    },{
        id       : 7,
        title    : 'Расчетно-дефектная ведомость',
        required : false
    },{
        id       : 8,
        title    : 'Акт браковки запасных частей',
        required : false
    },{
        id       : 9,
        title    : 'Справка 2730',
        required : false
    },{
        id       : 10,
        title    : 'Справка 2731',
        required : false
    },{
        id       : 11,
        title    : 'Акт приема-передач ТМЦ',
        required : false
    },{
        id       : 12,
        title    : 'Акт возврата ТМЦ',
        required : false
    },{
        id       : 13,
        title    : 'Акт формы ВУ-41',
        required : false
    },{
        id       : 14,
        title    : 'Заключение по случаям обнаружения (рекл)',
        required : false
    },{
        id       : 15,
        title    : 'Уведомление (рекл)',
        required : false
    },{
        id       : 16,
        title    : 'Фотографии (рекл)',
        required : false
    },{
        id       : 17,
        title    : 'Копии подтверждающих документов по затратам на приоретенные запчасти',
        required : false
    }
];
export const Operational: any[] = [
    {
        id       : 1,
        title    : 'Акт выполненных работ',
        required : false
    },
    {
        id       : 2,
        title    : 'Счет-фактура',
        required : false
    },{
        id       : 3,
        title    : 'Акт формы ВУ-36',
        required : false
    },{
        id       : 4,
        title    : 'Акт формы ВУ-23',
        required : false
    },{
        id       : 5,
        title    : 'Справка 2612',
        required : false
    },{
        id       : 6,
        title    : 'Дефектная ведомость ВУ-22',
        required : false
    },{
        id       : 7,
        title    : 'Расчетно-дефектная ведомость',
        required : false
    },{
        id       : 8,
        title    : 'Акт браковки запасных частей',
        required : false
    },{
        id       : 9,
        title    : 'Справка 2730',
        required : false
    },{
        id       : 10,
        title    : 'Справка 2731',
        required : false
    },{
        id       : 11,
        title    : 'Акт приема-передач ТМЦ',
        required : false
    },{
        id       : 12,
        title    : 'Акт возврата ТМЦ',
        required : false
    },{
        id       : 17,
        title    : 'Копии подтверждающих документов по затратам на приоретенные запчасти',
        required : false
    }
];
