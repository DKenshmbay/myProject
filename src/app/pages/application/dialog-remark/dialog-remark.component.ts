import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-remark',
  templateUrl: './dialog-remark.component.html',
  styleUrls: ['./dialog-remark.component.scss']
})
export class DialogRemarkComponent implements OnInit {
  dialogData:any
  constructor(public dialog: MatDialogRef<DialogRemarkComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.dialogData =  this.data;
  }
  onNoClick(): void {
    this.dialog.close();
  }
}
