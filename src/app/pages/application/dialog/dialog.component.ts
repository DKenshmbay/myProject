import { Component, OnInit, Optional, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApplicationService } from 'app/_services/application.service'
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  name: string;
  file:any;
  filesList:any;
  fromDialog: string;
  secondFile: boolean;
  @ViewChild('pdfViewer') public pdfViewer;
  @ViewChild('pdfViewerSecond') public pdfViewerSecond;

  constructor( 
    public dialogRef: MatDialogRef<DialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private appService:ApplicationService
    ) { 
      this.secondFile = false;
    }

  ngOnInit(): void {
    this.name = this.data.name;
    this.filesList = this.data.files;
    this.appService.getPdfFilesById(this.data.id).subscribe(a=> {
      this.pdfViewer.pdfSrc = a;
      this.pdfViewer.refresh();
    })
  }
  closeDialog() {
    this.dialogRef.close({ event: 'close', data: this.fromDialog });
  }
  openFile(data:any){
    this.secondFile = true;
    this.appService.getPdfFilesById(data.value).subscribe(a=> {
      this.pdfViewerSecond.pdfSrc = a; 
      this.pdfViewerSecond.refresh();
    })
  }
}
