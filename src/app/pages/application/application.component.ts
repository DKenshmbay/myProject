import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ApplicationService } from 'app/_services/application.service'
import { Applications } from 'app/interfaces/applications.interface'
import { MatSnackBar } from '@angular/material/snack-bar'; 
import { ActivatedRoute, Router } from '@angular/router';
import { first, take, takeUntil } from 'rxjs/operators';
import { AppComponent } from '../../app.component';
import { MatDialog } from '@angular/material/dialog';
import { UploadService } from 'app/_services/upload.service';
import { forkJoin, ReplaySubject, Subject } from 'rxjs';
import { DialogComponent } from './dialog/dialog.component';
import { DialogRemarkComponent } from './dialog-remark/dialog-remark.component';
import { Contractors } from '../../interfaces/contractors.interface';
import { MatSelect } from '@angular/material/select';
import { AuthenticationService } from 'app/_services';
import { Roles } from 'app/enums/roles.enum';
import { IDecodedToken } from 'app/interfaces/decoded-token.interface';
import { ApplicationStatus } from 'app/enums/applicationStatus.enum';
import { ApplicationTaskStatusEnum } from 'app/enums/ApplicationTaskStatusEnum.enum';
// import { Reclamation, Operational } from './fileTypeData';
import {MatAccordion} from '@angular/material/expansion';
import { result } from 'lodash';
// import { AppDateAdapter, APP_DATE_FORMATS } from 'app/pages/application/format-datepicker';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss'],
  providers: [{
    provide: MAT_DATE_LOCALE,
    useValue: 'ru-RU'
  },
  {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  },
  {
      provide: MAT_DATE_FORMATS,
      useValue: MY_FORMATS
  }
  // ,
  // {
  //   provide: DateAdapter, 
  //   useClass: AppDateAdapter
  // },
  // {
  //   provide: MAT_DATE_FORMATS, 
  //   useValue: APP_DATE_FORMATS
  // }
]
})

export class ApplicationComponent implements OnInit, AfterViewInit, OnDestroy{
    @ViewChild('accordion',{static:true}) Accordion: MatAccordion
    @ViewChild('file', { static: false }) file;
    public files: Set<File> = new Set();
    public fileTypeID:number;
    public get Roles(): typeof Roles {
        return Roles; 
      }
    minDate:Date = new Date();
    remarks:any;
    history:any;
    form: FormGroup;
    contractors:Contractors[];
    public contractorsFiltered;
    defects:any;
    public defectsFiltered;
    isEdit: boolean;
    canViewFile:boolean;
    canCounterpartySend:boolean | null;
    currentStatus:ApplicationStatus;
    canEditFile:boolean;
    sendToDocumentColOrAgreement:boolean | null;
    canEditApplication: boolean;
    canEditApplicationRadio: boolean;
    public dicDetachFiltered;
    dicDetach:any;
    formApplication:Applications;
    loadedFiles:any;
    id:string;
    fileTypes:any;
    repairPlace:any;
    public repairPlaceFiltered;
    public get Status(): typeof ApplicationStatus {
        return ApplicationStatus; 
    }
    public get Decision(): typeof ApplicationTaskStatusEnum {
        return ApplicationTaskStatusEnum; 
    }
    
    public currentUser: IDecodedToken;
    public currentUserRoles: string;
    public contractorsFilterCtrl: FormControl = new FormControl();
    protected _onDestroy = new Subject<void>();
    public filteredContractors: ReplaySubject<Contractors[]> = new ReplaySubject<Contractors[]>(1);
    //@ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
    
    constructor(
      private _formBuilder: FormBuilder, 
      private applicationService:ApplicationService,
      private snackBar:MatSnackBar,
      private route: ActivatedRoute,
      public myapp: AppComponent,
      public dialog: MatDialog,
      public uploadService: UploadService,
      private router:Router,
      private authenticationService: AuthenticationService
      ) { 
        this.authenticationService.currentUser.subscribe(x => 
            {
                this.currentUser = x,
                this.currentUserRoles = x.roles.toString();
            });
            this.canEditApplication = false;
            this.canViewFile = true;
            this.canEditFile = false;
            this.fileTypeID = 1;
       }
getEdit(_status:ApplicationStatus)
{
    debugger
    switch(_status) { 
        case ApplicationStatus.AgreementITSZHT: { 
            if(this.isInRole(Roles.AuditService))
                this.isEdit = true; 
           break; 
        } 
        case ApplicationStatus.AgreementOpd: { 
            if(this.isInRole(Roles.SalesDepartment))
            {
                this.isEdit = true; 
                this.canEditFile = true;
            }
           break; 
        } 
        case ApplicationStatus.AgreementTso: { 
            if(this.isInRole(Roles.Economist))
                this.isEdit = true; 
           break; 
        } 

        case ApplicationStatus.ReWork:
        case ApplicationStatus.DocumentCollect: 
        {
          if(this.isInRole(Roles.Counterparty))
          {
            this.isEdit = true; 
            this.canEditFile = true;
            this.sendToDocumentColOrAgreement = true;
            break;
          }
        }
        case ApplicationStatus.InRepair: 
        {
            if(this.isInRole(Roles.Counterparty))
            {
                this.isEdit = true; 
                this.sendToDocumentColOrAgreement = false;
                
            }

            break; 
        }
        case ApplicationStatus.PaymentFormation:
        {
            if(this.isInRole(Roles.TorManager)){
                this.isEdit = true;
                this.canEditFile = true;
                this.canViewFile = true;
            }
               
            break; 
        }
        case ApplicationStatus.Payment: 
        {
            if(this.isInRole(Roles.Treasurer))
                this.isEdit = true; 
            break; 
        }
        case ApplicationStatus.Draft:
            {
                if(this.isInRole(Roles.TorSpecialist)){
                  this.isEdit = true; 
                  this.canEditApplication = true;
                }
                    this.canViewFile = false;
                break; 
            } 
     } 
     if((_status == ApplicationStatus.InRepair && this.form.value.withReplacement==null))
         this.canCounterpartySend = false;

  }

  // repairChange(type:number){
  //   if(type==1){
  //     this.fileTypes = JSON.parse(JSON.stringify(Operational));
  //   }else if(type==2)
  //     this.fileTypes = JSON.parse(JSON.stringify(Reclamation));
  // }
  ngOnInit(): void {
    this.canEditApplication = false;
    this.canViewFile = true;
    this.canEditFile = false;
    this.fileTypeID = 1;
    this.sendToDocumentColOrAgreement = null;
      this.applicationService.getDictionary('DicContractors').subscribe(result => {
          this.contractors = result;
          this.contractorsFiltered = this.contractors.slice();
      });
      
      this.applicationService.getDictionary('DicDefect').subscribe(result => {
          this.defects = result;
          this.defectsFiltered = this.defects.slice();
      });
      
      this.applicationService.getDictionary('DicDetach').subscribe(result => {
          this.dicDetach = result;
          this.dicDetachFiltered = this.dicDetach.slice();
      });

      this.applicationService.getDictionary('DicRepairPlace').subscribe(result =>{
        this.repairPlace = result;
        this.repairPlaceFiltered = this.repairPlace.slice();
      });

      this.id = this.route.snapshot.paramMap.get('id');
      if(this.id){
          this.applicationService.getApplicationById(this.id)
          .pipe(first()).subscribe(x => 
            {
                this.form.patchValue(x.result);
                this.currentStatus = x.result.status;
                this.isEdit = false;
                this.getEdit(x.result.status);
                // this.repairChange(x.result.repairType)
            });
          this.applicationService.getApplicationFilesById(this.id).subscribe(x => this.loadedFiles=x)
        }
        else
        this.getEdit(ApplicationStatus.Draft);
      // Reactive Form
      this.form = this._formBuilder.group({
          id: [''],
          carriageNumber: [{value: '', disabled: !this.canEditApplication}, Validators.required],
          contractorsId: ['', Validators.required],
          defectId: ['', Validators.required],
          dicDetachId: ['', Validators.required],
          releaseDate: ['', Validators.required],
          description: [{value: '', disabled: !this.canEditApplication}],
          withReplacement: [''],
          repairType: [''],
          loadedMark: ['', Validators.required],
          number: [''],
          repairPlaceId: [''],
          coSign: ['']
      });

      this.loadRemarks();
  }
  isInRole(role:Roles)
  {
    if(this.currentUserRoles)
        return  JSON.parse(this.currentUserRoles).some(a=>a==role);
  }
  isInRoles(roles:Roles[])
  {
    if(this.currentUserRoles)
        return  JSON.parse(this.currentUserRoles).some(a=>roles.includes(a));
  }
  loadRemarks(){
    if (this.id)
    this.applicationService.getRemarks(this.id).subscribe(result => {
      this.remarks = result;

    this.applicationService.getApplicationHistory(this.id).subscribe(result => {
      this.history = result;
      });
    });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredContractors
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        //this.singleSelect.compareWith = (a: Contractors, b: Contractors) => a && b && a.id === b.id;
      });
  }

  save(){
      this.formApplication = this.form.getRawValue();
      if(this.validateCounterparty())
      {
        this.applicationService.sendAppliation(this.formApplication).subscribe(result => {
          this.canCounterpartySend = true
            this.form.value.id = result;
            this.myapp.updateBadgeCounts();
            this.snackBar.open('Заявка сохранена!', '', {
              duration: 2000,
            });
            this.router.navigate(['/application/edit/'+this.form.value.id]);
            
        });
      }
  }
  validateCounterparty()
  {
    if(this.currentStatus == ApplicationStatus.InRepair && this.form.value.withReplacement==null)
        {
            this.snackBar.open('Заполните поле Ремонт с заменой запасной части', '', {
                duration: 2000,
              });
              return false;
        }
        if(this.currentStatus == ApplicationStatus.InRepair && !this.form.value.repairType)
        {
            this.snackBar.open('Выберите тип ремонта: Эксплуатационный/Рекламационный', '', {
                duration: 2000,
              });
              return false;
        }
        return true;
  }    

  sendToUser(decision: ApplicationTaskStatusEnum)
  {
      debugger
    if(this.isInRole(Roles.Counterparty) && this.currentStatus == ApplicationStatus.DocumentCollect && this.loadedFiles.length==0){
      this.snackBar.open('Загрузите обязательные файлы!', '', {
        duration: 2000,
      });
      return;
    }
    else if(this.validateCounterparty()){
        this.formApplication = this.form.getRawValue();
        this.applicationService.sendAppliation(this.formApplication).subscribe(result => {
              this.form.value.id = result;
              this.applicationService.sendToUser({applicationId:this.form.value.id, decision:decision}).subscribe(result => {
                this.form.value.id = result;
                this.snackBar.open('Заявка отправлена!', '', {
                  duration: 2000,
                });
                this.myapp.updateBadgeCounts();
                this.ngOnInit();
            });
          });

    }
  }

  // checkFiles(){
  //   var result = true;
  //   for (let key in this.fileTypes) {
  //     if (this.fileTypes[key].required) {
  //         var tmp = this.loadedFiles.filter(item => item.fileType.toString().indexOf(this.fileTypes[key].id) !== -1);
  //         this.fileTypes[key].color = (tmp.length>0?'':'red');
  //         if(tmp.length==0){
  //           result = false;
  //         } 
  //     }
  //   }
  //   return result;
  // }
  progress;
  canBeClosed = true;
  primaryButtonText = 'Upload';
  showCancelButton = true;
  uploading = false;
  uploadSuccessful = false;

  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
  }

  addFiles() {
    this.file.nativeElement.click();
  }
  removeFile(file){
    this.applicationService.removeApplicationFile(file.id).subscribe(a=>console.log(a));
    const index: number = this.loadedFiles.indexOf(file);
    this.loadedFiles.splice(index, 1);
  }
  showFile(fileIn){
    const dialogRef = this.dialog.open(DialogComponent, {
      maxWidth: '95vw !important',
      width: '95%',
      maxHeight:'95vh',
      height:'95vh',
      data: {
        id: fileIn.id,
        name: fileIn.name,
        files: this.loadedFiles
      }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  UploadFiles() {

    // if everything was uploaded already, just close the dialog
    if (this.uploadSuccessful) {
      //return this.dialogRef.close();
    }

    // set the component state to "uploading"
    this.uploading = true;

    // start the upload and save the progress map
    this.progress = this.uploadService.upload(this.files,this.form.value.id,this.fileTypeID);
    for (const key in this.progress) {
      this.progress[key].progress.subscribe(val => console.log(val));
    }

    // convert the progress map into an array
    let allProgressObservables = [];
    for (let key in this.progress) {
      allProgressObservables.push(this.progress[key].progress);
    }

    // Adjust the state variables

    // The OK-button should have the text "Finish" now
    this.primaryButtonText = 'Finish';

    // The dialog should not be closed while uploading
    this.canBeClosed = false;
    //this.dialogRef.disableClose = true;

    // Hide the cancel-button
    this.showCancelButton = false;

    // When all progress-observables are completed...
    forkJoin(allProgressObservables).subscribe(end => {
      this.applicationService.getApplicationFilesById(this.id).subscribe(x => this.loadedFiles=x);
      this.files = new Set();
      // ... the dialog can be closed again...
      this.canBeClosed = true;
     // this.dialogRef.disableClose = false;

      // ... the upload was successful...
      this.uploadSuccessful = true;

      // ... and the component is no longer uploading
      this.uploading = false;
    });
  
  }

  addRemark(){
    const dialogRef = this.dialog.open(DialogRemarkComponent, {
      width: '50%',
      data: {text:'', applicationId:this.id, type:false}
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.applicationService.addRemark(result).subscribe(result => {
          this.snackBar.open('Замечание добавлено!', '', {
            duration: 2000,
          });
          this.loadRemarks();
       });
      }
    });
  }
  editRemark(item){
    item.type = true;
    const dialogRef = this.dialog.open(DialogRemarkComponent, {
      width: '50%',
      data: JSON.parse(JSON.stringify(item))
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        
        this.applicationService.addRemark(result).subscribe(added => {
          item.text = result.text;
          this.snackBar.open('Замечание сохранено!', '', {
            duration: 2000,
          });
       });
      }
    });
  }
  removeRemark(item){
    this.applicationService.removeRemarkById(item.id).subscribe(x => {
      const index: number = this.remarks.indexOf(item);
      this.remarks.splice(index, 1);
        this.snackBar.open('Замечание удалено!', '', {
            duration: 2000,
          });
    })
  }
  // openGroup(id:number){
  //   this.fileTypeID = id;
  //   this.files = new Set();
  // }
  showStatusTimeLine(status:number,prevStatus:number){
    let tmp = 'gray'
    let loop = true;
    if(this.history){
      for (let key in this.history) {
        if(loop){
          if (this.history[key].status==status) {
            tmp = 'green';
            loop = false;
          }else if(this.history[key].status==prevStatus){
            tmp = 'blue';
          }
        }
      }
    }
    return tmp;
  }
}
