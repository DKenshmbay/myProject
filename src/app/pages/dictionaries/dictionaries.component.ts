import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ApplicationStatus } from 'app/enums/applicationStatus.enum';
import { MatSnackBar } from '@angular/material/snack-bar'; 
import { AppComponent } from '../../app.component';
import { AuthenticationService } from 'app/_services';
import { Roles } from 'app/enums/roles.enum';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { DictionaryService } from 'app/_services/dictionary.service';
import { Dictionary } from '../../interfaces/dictionary.interface';
import { DictionaryComponent } from './dictionary/dictionary.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-dictionaries',
  templateUrl: './dictionaries.component.html',
  styleUrls: ['./dictionaries.component.scss']
})
export class DictionariesComponent implements AfterViewInit {
    // const paginatorIntl = new MatPaginatorIntl();
    
   
    displayedColumns: string[] = [ 'nameRu','options'];
    dataSource: MatTableDataSource<Dictionary>;
  
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    // rows: any[];

    rows: any[];
    currentStatus:ApplicationStatus;
    loadingIndicator: boolean;
    isEdit: boolean;
    reorderable: boolean;
    filter:string;
    public currentUserRoles: string;
    public get Roles(): typeof Roles {
        return Roles; 
      }
    public get Status(): typeof ApplicationStatus {
        return ApplicationStatus; 
    }
    /**
     * Constructor
     *
     */
    constructor(
        private dictionaryService: DictionaryService,
        private snackBar:MatSnackBar,
        public myapp: AppComponent,
        private authenticationService: AuthenticationService,
        public dialog: MatDialog,
    )
    {
        this.loadingIndicator = true;
        this.reorderable = true;
        this.authenticationService.currentUser.subscribe(x => 
            {
                if(x)
                this.currentUserRoles = x.roles.toString()
            });
    }
    isInRole(role:Roles)
    {
      if(this.currentUserRoles)
          return  JSON.parse(this.currentUserRoles).some(a=>a==role);
    }
    isInRoles(roles:Roles[])
    {
      if(this.currentUserRoles)
          return  JSON.parse(this.currentUserRoles).some(a=>roles.includes(a));
    }

    
    edit(item:any){
      const dialogRef = this.dialog.open(DictionaryComponent, {
        width: '50%',
        data: item,
      });
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.snackBar.open('Станция изменена!', '', {
            duration: 2000,
          });
          item.nameRu = result.data.name;
        }
      });
    }

    addNewStantion(){
      const dialogRef = this.dialog.open(DictionaryComponent, {
          width: '50%',
          data: {}
        });
      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.snackBar.open('Станция добавлена!', '', {
            duration: 2000,
          });
          this.filter = '';
          this.dataSource.filter  = '';
          this.loadDictionary();
        }
      });
    }

    delete(id:string){
      this.dictionaryService.deleteStation(id).subscribe(s =>{
        this.loadDictionary();
        this.snackBar.open('Станция удалена!', '', {
          duration: 2000,
        });
      })
    }

    /**
     * On init
     */
    ngAfterViewInit() {
        this.loadingIndicator = false;
    }
    ngOnInit(): void
    {
      this.loadDictionary();
    }

    loadDictionary(){
      this.dictionaryService.getAll().subscribe(result =>{
        this.dataSource = new MatTableDataSource(result);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }
}
