import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Dictionary } from 'app/interfaces/dictionary.interface';
import { DictionaryService } from 'app/_services/dictionary.service';
import { MatSnackBar } from '@angular/material/snack-bar'; 
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.scss']
})
export class DictionaryComponent implements OnInit {
  form: FormGroup;
  formDictionaries:Dictionary;

  constructor(
    public dialogRef: MatDialogRef<DictionaryComponent>,
    private _formBuilder: FormBuilder,
    private dictionaryService: DictionaryService,
    private snackBar:MatSnackBar,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
        id: [this.data?this.data.id:''],
        name:[this.data?this.data.nameRu:''],
    });
  }

  save(){
    this.formDictionaries = this.form.getRawValue();
      this.dictionaryService.addOrUpdate(this.formDictionaries).subscribe(result => {
       // this.form.value.id = result;
          this.dialogRef.close({ event: 'close', data: this.formDictionaries });
      });
  }
}
