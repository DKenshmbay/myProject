import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './auth/login/login.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';

import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { FuseSharedModule } from '@fuse/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AuthGuard } from '../_helpers';
import { AdminGuard } from '../_guards/admin.guard';
import { FormsModule } from '@angular/forms';
import { ApplicationComponent } from './application/application.component';
import { ProjectDashboardComponent } from '../pages/dashboards/project/project.component';
import { MatSnackBarModule} from '@angular/material/snack-bar'; 
import { ProjectDashboardService } from '../pages/dashboards/project/project.service';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';

import { UploadService } from 'app/_services/upload.service';
import { ExcelService } from 'app/_services';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { DialogComponent } from './application/dialog/dialog.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { Roles } from 'app/enums/roles.enum';
import { UserListComponent } from './admin/user-list.component';
import { DialogRemarkComponent } from './application/dialog-remark/dialog-remark.component';
import { AdminDialogComponent } from './admin/dialog/admin-dialog.component';




import {MatTooltipModule} from '@angular/material/tooltip';


import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import {MatExpansionModule} from '@angular/material/expansion';
import { FilterByIdPipe} from '../pipe/filterById.pipe';
import { MatSelectFilterModule } from 'mat-select-filter';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { getRussianPaginatorIntl } from './main/russian-paginator-intl';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { DictionariesComponent } from './dictionaries/dictionaries.component';
import { DictionaryComponent } from './dictionaries/dictionary/dictionary.component';
import { NumberDirective } from './application/numbers-only.directive';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { AutocompleteOffDirective } from './application/autocomplete-off.directive';
import { MatGridListModule } from '@angular/material/grid-list';
import { ContractComponent } from './contract/contract/contract.component';
import { ContractsComponent } from './contract/contracts.component'; 
import { PrivatOfficeComponent } from './privat-office/privat-office.component';
import { NewPasswordComponent } from './privat-office/new-password/new-password.component';

@NgModule({
  declarations: [
    MainComponent,
    LoginComponent,
    ResetPasswordComponent,
    ApplicationComponent,
    DialogComponent,
    UserListComponent,
    DialogRemarkComponent,
    AdminDialogComponent,
    FilterByIdPipe,
    DictionariesComponent,
    DictionaryComponent,
    NumberDirective,
    AutocompleteOffDirective,
    ContractComponent,
    ContractsComponent,
    PrivatOfficeComponent,
    NewPasswordComponent

  ],
  exports:[
    MatDialogModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule
  ],
  imports: [
    MatTooltipModule,
    FormsModule,
    CommonModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatIconModule,
    MatInputModule,
    FuseSharedModule,
    NgxDatatableModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTabsModule,
    MatListModule,
    MatRadioModule,
    MatProgressBarModule,
    MatDialogModule,
    NgxMatSelectSearchModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatDatepickerModule, 
    MatMomentDateModule,
    MatExpansionModule,
    MatSelectFilterModule,
    NgxChartsModule,
    MatSlideToggleModule,
    PdfJsViewerModule,
    MatGridListModule,
    RouterModule.forChild([
      { path: '', component: ProjectDashboardComponent,canActivate: [AuthGuard], resolve  : { data: ProjectDashboardService }},
      { path: 'auth/login', component: LoginComponent },
      { path: 'auth/reset-password', component: ResetPasswordComponent },
      { path: 'application/create', component: ApplicationComponent, canActivate: [AuthGuard]},
      { path: 'application/edit/:id', component: ApplicationComponent, canActivate: [AuthGuard] },
      { path: 'applications/All', component: MainComponent},
      { path: 'applications/InWork', component: MainComponent},
      { path: 'applications/DocumentCollect', component: MainComponent},
      { path: 'applications/AgreementOpd', component: MainComponent},
      { path: 'applications/AgreementTso', component: MainComponent},
      { path: 'applications/AgreementITSZHT', component: MainComponent},
      { path: 'applications/PaymentFormation', component: MainComponent},
      { path: 'applications/Payment', component: MainComponent},
      { path: 'applications/Paid', component: MainComponent},
      { path: 'applications/Draft', component: MainComponent},
      { path: 'applications/ReWork', component: MainComponent},
      { path: 'applications/InRepair', component: MainComponent},
      { path: 'applications/Archived', component: MainComponent},
      { path: 'admin', component: UserListComponent, canActivate: [AdminGuard], data: { roles: [Roles.Admin] }  },
      { path: 'dictionaries', component: DictionariesComponent, canActivate: [AdminGuard], data: { roles: [Roles.Admin] }  },
      { path: 'dictionary/create', component: DictionaryComponent, canActivate: [AuthGuard] },
      { path: 'dictionary/edit/:id', component: DictionaryComponent, canActivate: [AuthGuard] },
      { path: 'contract-list', component: ContractsComponent, canActivate: [AdminGuard], data: { roles: [Roles.TorManager] }  },
      { path: 'private', component: PrivatOfficeComponent},
    ]),
  ],
  providers: [UploadService,ExcelService,AuthGuard,{provide: LocationStrategy, useClass: HashLocationStrategy},
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },{ provide: MatPaginatorIntl, useValue: getRussianPaginatorIntl() }]
})
export class PagesModule { }
