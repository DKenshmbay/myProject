import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from 'app/_services/admin.service';
import { ApplicationService } from 'app/_services/application.service';
import { AdminDialogComponent } from './dialog/admin-dialog.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
    
    users: any[];
    repairPlaceData: any[];
    roles: any[];
    loadingIndicator: boolean;
    reorderable: boolean;
    dialogData: any
    /**
     * Constructor
     *
     */
    constructor(
        private router: Router,
        private route:ActivatedRoute,
        private adminService:AdminService,
        private applicationService: ApplicationService,
        private snackBar:MatSnackBar,
        public dialog: MatDialog,
    )
    {
        this.loadingIndicator = true;
        this.reorderable = true;
    }
    add(){
        const dialogRef = this.dialog.open(AdminDialogComponent, {
            width: '50%',
            data: {
              userData:{},
              repairPlaceData:this.repairPlaceData,
              roles:this.roles,
              isAdd:true
            }
          });
          dialogRef.afterClosed().subscribe(result => {
            if(result){
              this.adminService.addUser(result).subscribe(result => {
                this.snackBar.open('Пользователь добавлен!', '', {
                  duration: 2000,
                });
                this.loadData();
             });
            }
          });
    }

    edit(item:any){
        const dialogRef = this.dialog.open(AdminDialogComponent, {
          width: '50%',
          data: {userData:
          {
            login:item.login,
            email:item.email,
            lastName:item.lastName,
            firstName:item.firstName,
            middleName:item.middleName,
            roleId:item.roleId,
            contractorId:item.contractorId,
            isBlocked:item.isBlocked
          },
          repairPlaceData:this.repairPlaceData, roles:this.roles, isAdd:false}
        });
        dialogRef.afterClosed().subscribe(result => {
          if(result){
            this.adminService.updateUser(result).subscribe(added => {
              this.snackBar.open('Запись сохранено!', '', {
                duration: 2000,
              });
              this.loadData();
           });
          }
        });
    }


    remove(login:string){
        this.adminService.deleteUser(login).subscribe(x => {
            this.snackBar.open('Заявка удалена!', '', {
                duration: 2000,
              });
        this.loadData();
        })
    }
  ngOnInit(): void {
    this.loadRoles();
    this.loadDicRepairPlaces();
    this.loadData();
  }
  loadData(){
    this.adminService.getUsers().subscribe(result => {
        this.users = result;
        this.loadingIndicator = false;
    });
 }
 loadRoles(){
    this.adminService.getRoles().subscribe(result => {
        this.roles = result;
        this.loadingIndicator = false;
    });
 }
 loadDicRepairPlaces(){
    this.applicationService.getDictionary("DicContractors").subscribe(result => {
        this.repairPlaceData= result;
        this.loadingIndicator = false;
    });
 }
}
