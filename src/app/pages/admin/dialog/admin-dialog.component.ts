import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar'; 

@Component({
  selector: 'app-admin-dialog',
  templateUrl: './admin-dialog.component.html',
  styleUrls: ['./admin-dialog.component.scss']
})
export class AdminDialogComponent implements OnInit {
    dialogData:any;
    repairPlaces:any[];
    roles:any[];
    isAdd:boolean;
    form: FormGroup;

    constructor(public dialog: MatDialogRef<AdminDialogComponent>,
                private _formBuilder: FormBuilder, private snackBar:MatSnackBar,
                @Inject(MAT_DIALOG_DATA) public data: any) { }
  
    ngOnInit(): void {
      this.dialogData =  this.data.userData;
      this.repairPlaces = this.data.repairPlaceData;
      this.roles = this.data.roles;
      this.isAdd = this.data.isAdd;

      this.form = this._formBuilder.group({
        id: [''],
        fName: ['', Validators.required],
        lastName: ['',Validators.required],
        middleName: [''],
        login: ['',Validators.required],
        email: ['',Validators.required],
        role: ['',Validators.required],
        counterPart: [''],
        password: ['',Validators.required],
        block: [''],
    });
    }
    onNoClick(): void {
      this.dialog.close();
    }

    validate()
    {
      if(this.form.value.fName==null)
          {
              this.snackBar.open('Заполните поле', '', {
                  duration: 2000,
                });
                return false;
          }
          
          return true;
    }
}
