export interface Dictionary {
    id: string;
    name: string;
    nameKz: string;
    createdDate: Date;
    modifiedDate: Date;

}

