export interface Contracts {
    id: string;
    contractorId: string;
    number: number;
    singDate:Date;
    statusId: number;
    finisDate: Date;
    note: string;
}