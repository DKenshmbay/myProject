export interface Applications {
    id: string;
    userId: string;
    number: string;
    carriageNumber: number;
    repairPlaceId: string;
    contractorsId: string;
    status:number;
    defectId: string;
    releaseDate: Date;
    description: string;

    // dicDetachId: number;
    // withReplacement: number;
    // repairType: boolean;
    // loadedMark: number;
}

