import { Roles } from '../enums/roles.enum';
export interface IDecodedToken {
    sub: string,
    nbf: number,
    exp: number,
    iin: string,
    lastName: string,
    login: string,
    firstName: string,
    middleName: string,
    roles: Roles[],
    accessToken?: string,
    fullName?: string;
  }
